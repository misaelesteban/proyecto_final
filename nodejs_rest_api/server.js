// server.js

// base setup

var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/equitacion')
var equitacion = require('./app/models/equitacion');

// call the packages


var express = require('express')
var app = express();
var bodyParser = require('body-parser');

// config app to use bodyParser()

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8082; 

// routes for our api

var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('something is happen..');
  

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
  
  res.header('Access-Control-Allow-Headers', 'Content-Type');


	next();  // make sure we go to the next routes
});


// test route

router.get('/', function(req, res) {
 res.json({ message: 'yahoo!! welcome to our api !' });
});

// more routes for our API will happen here
router.route('/equitacion')
 
  // create a equitacion accessed at POST
  // http://localhost:8082/api/equitacion
 .post(function (req, res) {
 	var equitacion = new equitacion();
 	equitacion.name = req.body.name;
 	// save the equitacion and check for errors
 	equitacion.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'equitacion created !'});
 	});

 	// body...
 })
  
 // get all the equitacion (accessed at 
 // http://localhost:8082/api/equitacion)
 .get(function (req, res) {

 	// body...
 	equitacion.find(function(err, equitacion) 
 	{
 		if (err)
 			res.send(err);
 		res.json(equitacion);
 	});
 });

// on routes that end in /equitacion/:equitacion_id
router.route('/equitacion/:equitacion_id')
 // get the equitacion with that id 
 // accessed at GET 
 // http://localhost:8082/api/equitacion/:equitacion_id

 .get(function(req, res) {
 	// body...
 	equitacion.findById(req.params.equitacion_id, function(err, equitacion)
 	{
 		if (err)
 			res.send(err);
 		res.json(bear);

 	});
  })

 // update the equitacion with this id
 // accessed at PUT
 // http://localhost:8082/api/equitacion/:equitacion_id

 .put(function (req, res) {
 	// use our equitacion model to find the equitacion we want
 	equitacion.findById(req.params.equitacion_id, function(err, equitacion) {
 		if (err)
 			res.send(err);

 		// update the equitacion info
 		equitacion.name = req.body.name;

 		// save the equitacion

 		equitacion.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'equitacion updated !'});
 		});
 	});
  })

 // delete the equitacion with this id 
 // accessed at DELETE
 // http://localhost:8082/api/equitacion/:equitacion_id
 .delete(function (req, res) {
   equitacion.remove({
   	_id : req.params.equitacion_id
   }, function (err, equitacion) {
   	if (err)
   		res.send(err);
   	res.json({message: 'equitacion deleted !'});
   });

 });

 

// register our routes
app.use('/api', router);
//app.use(require('cors')());

// start the server

app.listen(port);
console.log('Magic happens on port: ' + port);
